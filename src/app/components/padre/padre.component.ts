import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  mensajePadre1="Los heroes"
  mensajePadre2="Los saiyanines"

  Anime = {
    nombre:'Goku',
    raza:'Sayayin',
    tipo:'anime'
  }

  Heroe = {
    nombre:'Superman',
    raza:'kriptoniano',
    tipo:'animado'
  }

  constructor() {
  
   }

  ngOnInit(): void {
  }

}
