import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() mensaje1!:string;
  @Input() mensaje2!:string;
  @Input() mensaje3!:any;
  @Input() mensaje4!:any;


  constructor() { }

  ngOnInit(): void {
  }

}
